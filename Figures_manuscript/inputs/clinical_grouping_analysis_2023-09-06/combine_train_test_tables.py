import pandas as pd

df_jass_all = pd.DataFrame()
df_jass_training = pd.read_csv('category_traitset_with_mean_train_jass.tsv',delimiter='\t')
df_jass_test = pd.read_csv('category_traitset_with_mean_test_jass.tsv',delimiter='\t')
df_jass_all = pd.concat([df_jass_all,df_jass_training],axis=0)
df_jass_all = pd.concat([df_jass_all,df_jass_test],axis=0)

df_jass_all = df_jass_all.fillna({'Joint':0,'Both':0,'Univariate':0,'None':0})
df_jass_all.reset_index(inplace=True)

trait_sets = []
index_to_drop = []
for i in df_jass_all.index:
    traits = df_jass_all.iloc[i].trait_set 
    traits = str(sorted(traits.split(' ')))
    if traits not in trait_sets:
        trait_sets.append(traits)
    else:
        index_to_drop.append(i)

print(len(index_to_drop))
df_jass_all_no_duplicates = df_jass_all.drop(index=index_to_drop)
df_jass_all_no_duplicates.reset_index(inplace=True)
df_jass_all_no_duplicates.to_csv('category_traitset_with_mean_all_jass_no_duplicates.tsv',na_rep='NA',sep='\t')
print('#unique trait sets=',len(df_jass_all_no_duplicates))
