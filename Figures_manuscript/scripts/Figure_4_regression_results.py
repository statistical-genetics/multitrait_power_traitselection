import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import seaborn as sns
import matplotlib.transforms as mtransforms
sns.set_style("whitegrid",{'grid.linestyle':'--'})

def jitterplot(x, data, ax, labels=None):
	cols = ['red','blue','green','orange','purple']
	np.random.seed(0)
	for c in range(len(data)):
		if labels != None:
			ax.plot(np.random.normal(x-0.08,0.08), data[c], '.',color=cols[c],markersize=12, markeredgecolor='k',label=labels[c])
		else:
			ax.plot(np.random.normal(x-0.08,0.08), data[c], '.',color=cols[c],markersize=12, markeredgecolor='k')


data_path = '../inputs/JASS_5CVdata-2023-08-01/'
features = ['k','log10_avg_distance_cor','log10_mean_gencov','avg_Neff','avg_h2_mixer','avg_perc_h2_diff_region'] 
names =  ['# of traits','$log_{10}\Delta_{\Sigma}$',r'$log_{10}\bar{\Sigma}_{g}$','mean $N_{eff}$','mean $h^2_{GWAS}$',r'mean %$h^2_u$']

plt.rcParams.update({'font.size':18,'font.family':'arial'})
fig = plt.figure(figsize=[15,8])
axs = fig.subplot_mosaic([['(A)','(B)','(C)']])
# Linear
df = pd.read_csv(data_path+"5foldCV_linear_summary_semilogadjust_log10_excCorVar.csv",delimiter=',')
ax = axs['(A)']
_= ax.boxplot([df['corRtrain_mv'].values, df['corRval_mv'].values],showfliers=False)
_= ax.axhline(np.median(df['corRval_mv'].values),linestyle='--',color='orange')
_= ax.axhline(0,linestyle='--',color='k')
_= ax.set_xticks(range(1,3))
_= ax.set_xticklabels(['training','validation'])
_= ax.set_ylabel("Pearson's correlation coefficient")
_= ax.set_title('Linear')
_= ax.set_ylim([0,1])
_= ax.yaxis.set_major_locator(ticker.MultipleLocator(0.1))
jitterplot(1,df['corRtrain_mv'],ax)
jitterplot(2,df['corRval_mv'],ax)

# SVR
#model_type = 'SVR'
#df = pd.read_csv(data_path+"5foldCV_{}_summary_semilogadjust_log10_excCorVar.csv".format(model_type),delimiter=',')
#hyperparams = ['C','epsilon'] # kernel is not numeric. all 5 results were 'rbf'. degree does not matter for 'rbf' 
#ax = axs['(B)']
#_= ax.boxplot([df['corRtrain_mv'].values, df['corRval_mv'].values],showfliers=False)
#_= ax.axhline(np.median(df['corRval_mv'].values),linestyle='--',color='orange')
#_= ax.axhline(0,linestyle='--',color='k')
#_= ax.set_xticks(range(1,3),['training','validation'])
#_= ax.set_title('SVR')
#_= ax.set_ylim([0,1])
#_= ax.yaxis.set_major_locator(ticker.MultipleLocator(0.1))
#jitterplot(1,df['corRtrain_mv'],ax)
#jitterplot(2,df['corRval_mv'],ax)

# RFR
#model_type = 'RFR'
#df = pd.read_csv(data_path+"5foldCV_{}_summary_semilogadjust_log10_excCorVar.csv".format(model_type),delimiter=',')
#hyperparams = ['n_estimators','max_depth','min_samples_split','min_samples_leaf'] # max_features and bootstrap parameters are not numeric. all of them were 'sqrt' and 'False'
#ax = axs['(C)']
#_= ax.boxplot([df['corRtrain_mv'].values, df['corRval_mv'].values],showfliers=False)
#_= ax.axhline(np.median(df['corRval_mv'].values),linestyle='--',color='orange')
#_= ax.axhline(0,linestyle='--',color='k')
#_= ax.set_xticks(range(1,3))
#_= ax.set_xticklabels(['training','validation'])
#_= ax.set_title('RFR')
#_= ax.set_ylim([0,1])
#_= ax.yaxis.set_major_locator(ticker.MultipleLocator(0.1))
#jitterplot(1,df['corRtrain_mv'],ax)
#jitterplot(2,df['corRval_mv'],ax)

# Linear
df = pd.read_csv(data_path+"5foldCV_linear_summary_semilogadjust_log10_excCorVar.csv",delimiter=',')
ax = axs['(B)']
_= ax.boxplot([df[i+'_coef_mv'].values for i in features],showfliers=False)
_= ax.set_xticks(range(1,len(features)+1))
_= ax.set_xticklabels(names)
plt.setp(ax.get_xticklabels(), rotation=60, ha="right",rotation_mode="anchor")
_= ax.set_title('coefficients')
_= ax.axhline(0,linestyle='--',color='k')
for i,name in enumerate(features):
        jitterplot(i+1,df[name+'_coef_mv'],ax)

#ax = axs['(E)']
#_= ax.boxplot([df[i+'_coef_uv'].values for i in features],showfliers=False)
#_= ax.set_xticks(range(1,len(features)+1))
#_= ax.set_xticklabels(names)
#plt.setp(ax.get_xticklabels(), rotation=60, ha="right",rotation_mode="anchor")
#_= ax.set_title('coefficients\n(univariate)')
#_= ax.axhline(0,linestyle='--',color='k')
#for i,name in enumerate(features):
#        jitterplot(i+1,df[name+'_coef_uv'],ax)

ax = axs['(C)']
_= ax.boxplot([-np.log10(df[i+'_pval_mv'].values) for i in features],showfliers=False)
_= ax.set_xticks(range(1,len(features)+1))
_= ax.set_xticklabels(names)
plt.setp(ax.get_xticklabels(), rotation=60, ha="right",rotation_mode="anchor")
_= ax.set_title('$-log_{10}$p-values')
#_= ax.set_ylim([0,50])
_= ax.axhline(-np.log10(0.05/len(features)),linestyle='--',color='r')
for i,name in enumerate(features):
	if i < len(features)-1:
        	jitterplot(i+1,-np.log10(df[name+'_pval_mv']),ax)
	else:
		jitterplot(i+1,-np.log10(df[name+'_pval_mv']),ax,labels=['CV1','CV2','CV3','CV4','CV5'])
ax.legend(bbox_to_anchor=(1.6,1))

#ax = axs['(G)']
#_= ax.boxplot([-np.log10(df[i+'_pval_uv'].values) for i in features],showfliers=False)
#_= ax.set_xticks(range(1,len(features)+1))
#_= ax.set_xticklabels(names)
#plt.setp(ax.get_xticklabels(), rotation=60, ha="right",rotation_mode="anchor")
#_= ax.set_title('$-log_{10}$p-values\n(univariate)')
##_= ax.set_ylim([0,170])
#_= ax.axhline(-np.log10(0.05/len(features)),linestyle='--',color='r')
#for i,name in enumerate(features):
#        jitterplot(i+1,-np.log10(df[name+'_pval_uv']),ax)

for label, ax in axs.items():
	# label physical distance to the left and up:
	trans = mtransforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
	ax.text(0.0, 1.0, label, transform=ax.transAxes + trans,
		fontsize='medium', va='bottom', fontfamily='arial')

fig.subplots_adjust(wspace=0.35,bottom=0.35,hspace=0.35,left=0.1,right=0.8)
fig.savefig('../outputs/Figure_4_regression_results.png',dpi=300)
