import pandas as pd
import matplotlib
matplotlib.use('AGG')
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import pearsonr,spearmanr
from sklearn.metrics import r2_score

plt.rcParams.update({'font.size':18,'font.family':'arial'})

infile = 'gain-true-predicted_jass_linear_pre-selected-features'
outfile = 'Figure_SUP_obs-pred-gain_jass.png'
#infile = 'gain-true-predicted_mtag_linear_pre-selected-features'
#outfile = 'Figure_SUP_obs-pred-gain_mtag.png'

path_name_output='../outputs/'

y_val_scaled_all = []
y_pred_all = []
fig,axs=plt.subplots(1,5,figsize=[20,5])
labels = ['(A)','(B)','(C)','(D)','(E)']
for i in [2,3,4,5,6]:
    df = pd.read_csv('../inputs/JASS_true_pred_gains-2023-08-03/'+infile+'_CV{}.txt'.format(i),delimiter=',')
    y_val_scaled = df['observed_gain_scaled'].values
    y_pred = df['predicted_gain_scaled'].values
    ax = axs[i-2]
    ax.plot(y_pred,y_val_scaled,'.',label='CV{}'.format(i),alpha=0.3)
    rho,pval=pearsonr(y_pred,y_val_scaled) 
    ax.text(0.6,0,r'r={}'.format(round(rho,2)))
#    ax.text(0.5,0,r'($p$={})'.format('%.1E' % pval),fontsize=15)
#    ax.plot(df['adjusted_predicted_gain_scaled'].values,y_val_scaled,'.',label='CV{}'.format(i),alpha=0.3)
    if i == 4:
        ax.set_xlabel('predicted gain')
    if i == 2:
        ax.set_ylabel('observed gain (validation)')
    x=np.linspace(0,1,100)
    ax.plot(x,x,'--',color='k')
    ax.axhline(y=np.mean(y_val_scaled),linestyle='-',color='grey')
    ax.axvline(x=np.mean(y_pred),linestyle='-',color='grey')
#    ax.axvline(x=np.mean(df['adjusted_predicted_gain_scaled'].values),linestyle='-',color='grey')
    ax.set_title('{} CV{}'.format(labels[i-2],i))
#    ax.set_xlim([0,1.5])
#    ax.set_ylim([0,1])
    y_val_scaled_all += y_val_scaled.tolist()
    y_pred_all += y_pred.tolist()

print(pearsonr(y_val_scaled_all,y_pred_all))
print(spearmanr(y_val_scaled_all,y_pred_all))

plt.subplots_adjust(bottom=0.3,wspace=0.3)
plt.savefig(path_name_output+outfile,dpi=300)
