import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import seaborn as sns
import matplotlib.transforms as mtransforms
sns.set_style("whitegrid",{'grid.linestyle':'--'})

def jitterplot(x, data, ax, labels=None):
	cols = ['red','blue','green','orange','purple']
	np.random.seed(0)
	for c in range(len(data)):
		if labels != None:
			ax.plot(np.random.normal(x-0.08,0.08), data[c], '.',color=cols[c],markersize=12, markeredgecolor='k',label=labels[c])
		else:
			ax.plot(np.random.normal(x-0.08,0.08), data[c], '.',color=cols[c],markersize=12, markeredgecolor='k')


plt.rcParams.update({'font.size':18,'font.family':'arial'})
fig = plt.figure(figsize=[15,15],constrained_layout=True)
axs = fig.subplot_mosaic([['(A)','(B)','(C)'],
			    ['(D)','(E)','(F)']])

df = pd.read_csv("../inputs/JASS_5CVdata-2023-08-01/5foldCV_linear_summary_semilogadjust_log10_excCorVar_wPI.csv",delimiter=',')
df_svr = pd.read_csv("../inputs/JASS_5CVdata-2023-08-01/5foldCV_SVR_summary_semilogadjust_log10_excCorVar_wPI.csv",delimiter=',')
df_rfr = pd.read_csv("../inputs/JASS_5CVdata-2023-08-01/5foldCV_RFR_summary_semilogadjust_log10_excCorVar_wPI.csv",delimiter=',')
features = ['k','log10_avg_distance_cor','log10_mean_gencov','avg_Neff','avg_h2_mixer','avg_log10pi_semilogadjust']
names =  ['# of traits','$log_{10}\Delta_{\Sigma}$',r'$log_{10}\bar{\Sigma}_{g}$','mean $N_{eff}$','mean $h^2_{GWAS}$','mean($log_{10}$(polygenicity))']
# Linear
ax = axs['(A)']
_= ax.boxplot([df['corRtrain_mv'].values, df['corRval_mv'].values],showfliers=False)
_= ax.axhline(np.median(df['corRval_mv'].values),linestyle='--',color='orange')
_= ax.axhline(0,linestyle='--',color='k')
_= ax.set_xticks(range(1,3))
_= ax.set_xticklabels(['training','validation'])
_= ax.set_ylabel("Pearson's correlation")
_= ax.set_title('With $mean(log_{10}polygenicity))$',**{'fontsize':15})
_= ax.set_ylim([0,1])
_= ax.yaxis.set_major_locator(ticker.MultipleLocator(0.2))
jitterplot(1,df['corRtrain_mv'],ax)
jitterplot(2,df['corRval_mv'],ax)

# SVR
#hyperparams = ['C','epsilon'] # kernel is not numeric. all 5 results were 'rbf'. degree does not matter for 'rbf' 
#ax = axs['(B)']
#_= ax.boxplot([df_svr['corRtrain_mv'].values, df_svr['corRval_mv'].values],showfliers=False)
#_= ax.axhline(np.median(df_svr['corRval_mv'].values),linestyle='--',color='orange')
#_= ax.axhline(0,linestyle='--',color='k')
#_= ax.set_xticks(range(1,3),['training','validation'])
#_= ax.set_title('SVR')
#_= ax.set_ylim([0,1])
#_= ax.yaxis.set_major_locator(ticker.MultipleLocator(0.1))
#jitterplot(1,df_svr['corRtrain_mv'],ax)
#jitterplot(2,df_svr['corRval_mv'],ax)

# RFR
#hyperparams = ['n_estimators','max_depth','min_samples_split','min_samples_leaf'] # max_features and bootstrap parameters are not numeric. all of them were 'sqrt' and 'False'
#ax = axs['(C)']
#_= ax.boxplot([df_rfr['corRtrain_mv'].values, df_rfr['corRval_mv'].values],showfliers=False)
#_= ax.axhline(np.median(df_rfr['corRval_mv'].values),linestyle='--',color='orange')
#_= ax.axhline(0,linestyle='--',color='k')
#_= ax.set_xticks(range(1,3))
#_= ax.set_xticklabels(['training','validation'])
#_= ax.set_title('RFR')
#_= ax.set_ylim([0,1])
#_= ax.yaxis.set_major_locator(ticker.MultipleLocator(0.1))
#jitterplot(1,df_rfr['corRtrain_mv'],ax)
#jitterplot(2,df_rfr['corRval_mv'],ax)

# Linear
ax = axs['(B)']
_= ax.boxplot([df[i+'_coef_mv'].values for i in features],showfliers=False)
_= ax.set_xticks([])
_= ax.set_xticks(range(1,len(features)+1))
_= ax.set_xticklabels(names)
plt.setp(ax.get_xticklabels(), rotation=60, ha="right",rotation_mode="anchor")
_= ax.set_title('coefficients')
_= ax.axhline(0,linestyle='--',color='k')
for i,name in enumerate(features):
        jitterplot(i+1,df[name+'_coef_mv'],ax)

ax = axs['(C)']
_= ax.boxplot([-np.log10(df[i+'_pval_mv'].values) for i in features],showfliers=False)
_= ax.set_xticks(range(1,len(features)+1))
_= ax.set_xticklabels(names)
plt.setp(ax.get_xticklabels(), rotation=60, ha="right",rotation_mode="anchor")
_= ax.set_title('$-log_{10}$p-values')
#_= ax.set_ylim([0,50])
_= ax.axhline(-np.log10(0.05/len(features)),linestyle='--',color='r')
for i,name in enumerate(features):
        jitterplot(i+1,-np.log10(df[name+'_pval_mv']),ax)

### Mean effect size
df = pd.read_csv("../inputs/JASS_5CVdata-2023-08-01/5foldCV_linear_summary_semilogadjust_log10_excCorVar_wMES.csv",delimiter=',')
df_svr = pd.read_csv("../inputs/JASS_5CVdata-2023-08-01/5foldCV_SVR_summary_semilogadjust_log10_excCorVar_wMES.csv",delimiter=',')
df_rfr = pd.read_csv("../inputs/JASS_5CVdata-2023-08-01/5foldCV_RFR_summary_semilogadjust_log10_excCorVar_wMES.csv",delimiter=',')
features = ['k','log10_avg_distance_cor','log10_mean_gencov','avg_Neff','avg_h2_mixer','avg_log10mes_semilogadjust']
names = ['# of traits','$log_{10}\Delta_{\Sigma}$',r'$log_{10}\bar{\Sigma}_{g}$','mean $N_{eff}$','mean $h^2_{GWAS}$','mean($log_{10}$(mean effect size))']
# Linear
ax = axs['(D)']
_= ax.boxplot([df['corRtrain_mv'].values, df['corRval_mv'].values],showfliers=False)
_= ax.axhline(np.median(df['corRval_mv'].values),linestyle='--',color='orange')
_= ax.axhline(0,linestyle='--',color='k')
_= ax.set_xticks(range(1,3))
_= ax.set_xticklabels(['training','validation'])
_= ax.set_ylabel("Pearson's correlation")
_= ax.set_title('With $mean(log_{10}MES))$',**{'fontsize':15})
_= ax.set_ylim([0,1])
_= ax.yaxis.set_major_locator(ticker.MultipleLocator(0.2))
jitterplot(1,df['corRtrain_mv'],ax)
jitterplot(2,df['corRval_mv'],ax)

# SVR
#hyperparams = ['C','epsilon'] # kernel is not numeric. all 5 results were 'rbf'. degree does not matter for 'rbf' 
#ax = axs['(I)']
#_= ax.boxplot([df_svr['corRtrain_mv'].values, df_svr['corRval_mv'].values],showfliers=False)
#_= ax.axhline(np.median(df_svr['corRval_mv'].values),linestyle='--',color='orange')
#_= ax.axhline(0,linestyle='--',color='k')
#_= ax.set_xticks(range(1,3),['training','validation'])
#_= ax.set_title('SVR')
#_= ax.set_ylim([0,1])
#_= ax.yaxis.set_major_locator(ticker.MultipleLocator(0.1))
#jitterplot(1,df_svr['corRtrain_mv'],ax)
#jitterplot(2,df_svr['corRval_mv'],ax)

# RFR
#hyperparams = ['n_estimators','max_depth','min_samples_split','min_samples_leaf'] # max_features and bootstrap parameters are not numeric. all of them were 'sqrt' and 'False'
#ax = axs['(J)']
#_= ax.boxplot([df_rfr['corRtrain_mv'].values, df_rfr['corRval_mv'].values],showfliers=False)
#_= ax.axhline(np.median(df_rfr['corRval_mv'].values),linestyle='--',color='orange')
#_= ax.axhline(0,linestyle='--',color='k')
#_= ax.set_xticks(range(1,3))
#_= ax.set_xticklabels(['training','validation'])
#_= ax.set_title('RFR')
#_= ax.set_ylim([0,1])
#_= ax.yaxis.set_major_locator(ticker.MultipleLocator(0.1))
#jitterplot(1,df_rfr['corRtrain_mv'],ax)
#jitterplot(2,df_rfr['corRval_mv'],ax)

# Linear
ax = axs['(E)']
_= ax.boxplot([df[i+'_coef_mv'].values for i in features],showfliers=False)
_= ax.set_xticks(range(1,len(features)+1))
_= ax.set_xticklabels(names)
plt.setp(ax.get_xticklabels(), rotation=60, ha="right",rotation_mode="anchor")
_= ax.set_title('coefficients')
_= ax.axhline(0,linestyle='--',color='k')
for i,name in enumerate(features):
        jitterplot(i+1,df[name+'_coef_mv'],ax)

ax = axs['(F)']
_= ax.boxplot([-np.log10(df[i+'_pval_mv'].values) for i in features],showfliers=False)
_= ax.set_xticks(range(1,len(features)+1))
_= ax.set_xticklabels(names)
plt.setp(ax.get_xticklabels(), rotation=60, ha="right",rotation_mode="anchor")
_= ax.set_title('$-log_{10}$p-values')
#_= ax.set_ylim([0,50])
_= ax.axhline(-np.log10(0.05/len(features)),linestyle='--',color='r')
for i,name in enumerate(features):
	if i < len(features)-1:
        	jitterplot(i+1,-np.log10(df[name+'_pval_mv']),ax)
	else:
        	jitterplot(i+1,-np.log10(df[name+'_pval_mv']),ax,labels=['CV1','CV2','CV3','CV4','CV5'])
		

ax.legend(bbox_to_anchor=(1.6,1))


# add labels
for label, ax in axs.items():
	# label physical distance to the left and up:
	trans = mtransforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
	ax.text(0.0, 1.0, label, transform=ax.transAxes + trans,
		fontsize='medium', va='bottom', fontfamily='arial')

#fig.subplots_adjust(wspace=0.35,bottom=0.35,hspace=0.35,right=0.8,left=0.1)
fig.savefig('../outputs/Figure_SUP_regression_results_MES-PIversions.png',dpi=300)
