import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

df = pd.read_csv('../../Tables/Supp/Output/72trait_data_2023-06-09.csv',delimiter=',')

### global image setting
plt.rcParams.update({'font.size':'18','font.family':'arial'})
plt.figure(figsize=[8,6])

### Plot
sns.scatterplot(y='h2GWAS_mixer',x='h2_LD',data=df)
X = np.linspace(0,0.5,100)
Y = np.linspace(0,0.5,100)
plt.plot(X,Y,'k--')
plt.tight_layout()
plt.xlabel('$h^2_{GWAS}$ by LD score regression')
plt.ylabel('$h^2_{GWAS}$ by MiXeR')
cor = round(np.corrcoef(df.h2GWAS_mixer.values.astype('float'),df.h2_LD.values.astype('float'))[0,1],2)
plt.text(0,0.5,"Pearson's r={}".format(cor))
plt.savefig('../outputs/Figure_SUP_h2comparison.png',dpi=300)
