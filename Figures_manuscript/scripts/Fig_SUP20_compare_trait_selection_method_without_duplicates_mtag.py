import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('AGG')
import matplotlib.pyplot as plt
from scipy.stats import ttest_ind
from itertools import combinations
import matplotlib.transforms as mtransforms
import scipy

# MTAG data
df_train = pd.read_csv('../inputs/clinical_grouping_analysis_2023-09-06/category_traitset_with_mean_train_mtag.tsv',delimiter='\t')
df_val = pd.read_csv('../inputs/clinical_grouping_analysis_2023-09-06/category_traitset_with_mean_test_mtag.tsv',delimiter='\t')
output = '../outputs/Figure_SUP_compare_trait_selection_method_jass_without_duplicates_mtag.png' 
similar_train = df_train.loc[df_train.n_group==1]
dif_train = df_train.loc[(df_train.n_group>1)&(df_train.n_group<=4)]
verydif_train = df_train.loc[df_train.n_group>4]
similar_val = df_val.loc[df_val.n_group==1]
dif_val = df_val.loc[(df_val.n_group>1)&(df_val.n_group<=4)]
verydif_val = df_val.loc[df_val.n_group>4]
#datadriven = df_val.loc[df_val.rank_datadriven < 20]
datadriven = df_val.loc[df_val.rank_datadriven < df_val.rank_datadriven.nsmallest(101).iloc[-1]]

# JASS data
df_train_jass = pd.read_csv('../inputs/clinical_grouping_analysis_2023-09-06/category_traitset_with_mean_train_jass.tsv',delimiter='\t')
df_val_jass = pd.read_csv('../inputs/clinical_grouping_analysis_2023-09-06/category_traitset_with_mean_test_jass.tsv',delimiter='\t')
output = '../outputs/Figure_SUP_compare_trait_selection_method_jass_without_duplicates_mtag.png'
similar_train_jass = df_train_jass.loc[df_train_jass.n_group==1]
similar_val_jass = df_val_jass.loc[df_val_jass.n_group==1]

# I have edited the previous process where I dropped duplicates already, though only those that were duplicated within each of training and validation data.
#trait_sets = []
#for t in datadriven.trait_set:
#	traits = str(sorted(t.split(' ')))
#	if traits in trait_sets:
#		print(t)
#	else:
#		trait_sets.append(traits)
#print(len(trait_sets))
#
#print(datadriven.loc[(datadriven.trait_set=='z_MEGASTROKE_AIS z_MEGASTROKE_AS')|(datadriven.trait_set=='z_MEGASTROKE_AS z_MEGASTROKE_AIS')])
#datadriven = datadriven.loc[~datadriven.trait_set.isin(['z_MEGASTROKE_AIS z_MEGASTROKE_AS','z_MEGASTROKE_AS z_MEGASTROKE_AIS'])]
#datadriven.reset_index(inplace=True,drop=True)
#print(datadriven)
#datadriven.loc[len(datadriven),:] = df_val.loc[df_val.trait_set.isin(['z_MEGASTROKE_AIS z_MEGASTROKE_AS','z_MEGASTROKE_AS z_MEGASTROKE_AIS'])].values[0]
datadriven.reset_index(inplace=True,drop=True)
print(len(datadriven))
	

target = 'fraction_more_significant_joint_qval'
plt.rcParams.update({'font.size':18})
fig = plt.figure(figsize=[30,40])
axs = fig.subplot_mosaic([['(A)','(B)'],
                           ['(C)','(D)'],
			   ['(E)','(F)']])

ax=axs['(A)']
ax.boxplot([verydif_val['obs_gain'].values,dif_val['obs_gain'],similar_val['obs_gain'],datadriven['obs_gain']],notch=True)
ax.set_xticks([1,2,3,4])
ax.set_xticklabels(['High heterogeneity\n(>4 groups; {} sets)'.format(len(verydif_val)),'Low heterogeneity\n (2-4 groups; {} sets)'.format(len(dif_val)),'Homogenous\n (1 group; {} sets)'.format(len(similar_val)),'data-driven\n (top {} sets)'.format(len(datadriven))],rotation=30)
ax.set_ylim([-0.01,1.5])
ax.set_ylabel('observed gain')
result = ttest_ind(dif_val['obs_gain'].values,verydif_train['obs_gain'].values,axis=0,equal_var=False,alternative='two-sided')
ax.plot([1, 1, 2-0.01, 2-0.01],[1.09, 1.1, 1.1, 1.09], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(dif_val['obs_gain'].mean()-verydif_train['obs_gain'].mean(),2), '%.1E' % result.pvalue)
ax.text(1.5, 1.11, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)
result = ttest_ind(similar_val['obs_gain'].values,dif_train['obs_gain'].values,axis=0,equal_var=False,alternative='two-sided')
ax.plot([2+0.01, 2+0.01, 3-0.01, 3-0.01],[1.09, 1.1, 1.1, 1.09], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(similar_val['obs_gain'].mean()-dif_train['obs_gain'].mean(),2),'%.1E' % result.pvalue)
ax.text(2.5, 1.11, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)
result = ttest_ind(datadriven['obs_gain'].values,similar_train['obs_gain'].values,axis=0,equal_var=False,alternative='two-sided')
ax.plot([3+0.01, 3+0.01, 4-0.01, 4-0.01],[1.09, 1.1, 1.1, 1.09], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(datadriven['obs_gain'].mean()-similar_train['obs_gain'].mean(),2),'%.1E' % result.pvalue)
ax.text(3.5, 1.11, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)
result = ttest_ind(datadriven['obs_gain'].values,dif_train['obs_gain'].values,axis=0,equal_var=False,alternative='two-sided')
ax.plot([2, 2, 4, 4],[1.19, 1.2, 1.2, 1.19], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(datadriven['obs_gain'].mean()-dif_train['obs_gain'].mean(),2),'%.1E' % result.pvalue)
ax.text(3, 1.21, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)
result = ttest_ind(similar_val['obs_gain'].values,verydif_train['obs_gain'].values,axis=0,equal_var=False,alternative='two-sided')
ax.plot([1, 1, 3, 3],[1.29, 1.3, 1.3, 1.29], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(similar_val['obs_gain'].mean()-verydif_train['obs_gain'].mean(),2), '%.1E' % result.pvalue)
ax.text(2, 1.31, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)
result = ttest_ind(datadriven['obs_gain'].values,verydif_train['obs_gain'].values,axis=0,equal_var=False,alternative='two-sided')
ax.plot([1, 1, 4, 4],[1.39, 1.4, 1.4, 1.39], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(datadriven['obs_gain'].mean()-verydif_train['obs_gain'].mean(),2),'%.1E' % result.pvalue)
ax.text(2.5, 1.41, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)

# Joint
ax=axs['(B)']
ax.boxplot([verydif_val['obs_joint'].values,dif_val['obs_joint'].values,similar_val['obs_joint'].values,datadriven['obs_joint'].values],notch=True)
ax.set_xticks([1,2,3,4])
ax.set_xticklabels(['High heterogeneity\n (>4 groups; {} sets)'.format(len(verydif_val)),'Low heterogeneity\n (2-4 groups; {} sets)'.format(len(dif_val)),'Homogenous\n (1 group; {} sets)'.format(len(similar_val)),'data-driven\n (top {} sets)'.format(len(datadriven))],rotation=30)
ax.set_ylim([0,250])
ax.set_ylabel('observed #new associations')
result = ttest_ind(dif_val['obs_joint'].values,verydif_train['obs_joint'].values,axis=0,equal_var=False,alternative='two-sided')
ax.plot([1, 1, 2-0.01, 2-0.01],[165, 170, 170, 165], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(dif_val['obs_joint'].mean()-verydif_train['obs_joint'].mean(),2), '%.1E' % result.pvalue)
ax.text(1.5, 171, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)
result = ttest_ind(similar_val['obs_joint'].values,dif_train['obs_joint'].values,axis=0,equal_var=False,alternative='two-sided')
print(similar_val['obs_joint'].mean()-dif_train['obs_joint'].mean(),result.pvalue)
ax.plot([2+0.01, 2+0.01, 3-0.01, 3-0.01],[165, 170, 170, 165], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(similar_val['obs_joint'].mean()-dif_train['obs_joint'].mean(),2),'%.1E' % result.pvalue)
ax.text(2.5, 171, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)
result = ttest_ind(datadriven['obs_joint'].values,similar_train['obs_joint'].values,axis=0,equal_var=False,alternative='two-sided')
ax.plot([3+0.01, 3+0.01, 4-0.01, 4-0.01],[165, 170, 170, 165], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(datadriven['obs_joint'].mean()-similar_train['obs_joint'].mean(),2),'%.1E' % result.pvalue)
ax.text(3.5, 171, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)
result = ttest_ind(datadriven['obs_joint'].values,dif_train['obs_joint'].values,axis=0,equal_var=False,alternative='two-sided')
ax.plot([2, 2, 4, 4],[185, 190, 190, 185], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(datadriven['obs_joint'].mean()-dif_train['obs_joint'].mean(),2),'%.1E' % result.pvalue)
ax.text(3, 191, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)
result = ttest_ind(similar_val['obs_joint'].values,verydif_train['obs_joint'].values,axis=0,equal_var=False,alternative='two-sided')
ax.plot([1, 1, 3, 3],[205, 210, 210, 205], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(similar_val['obs_joint'].mean()-verydif_train['obs_joint'].mean(),2),'%.1E' % result.pvalue)
ax.text(2, 211, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)
result = ttest_ind(datadriven['obs_joint'].values,verydif_train['obs_joint'].values,axis=0,equal_var=False,alternative='two-sided')
ax.plot([1, 1, 4, 4],[225, 230, 230, 225], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(datadriven['obs_joint'].mean()-verydif_train['obs_joint'].mean(),2),'%.1E' % result.pvalue)
ax.text(2.5, 231, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)

clinical_name = {'[1]':'Neoplasm','[2]':'Cardiovascular Diseases','[3]':'Musculoskeletal and Neural\n Physiological Phenomena','[4]':'Psychological Phenomena','[5]':'Physiological Phenomena','[6]':'Nutritional and Metabolic Diseases','[7]':'Circulatory and Respiratory\n Physiological Phenomena','[8]':'Nervous System Diseases','[9]':'Mental disorders','[10]':'Immune System Diseases','[11]':'Reproductive and Urinary\n Physiological Phenomena','[12]':'Eye Diseases','[13]':'Population Characteristics','[14]':'Enzymes and Coenzymes'}

ax=axs['(C)']
ax.boxplot([similar_val['obs_gain'].values,similar_val_jass['obs_gain'].values],notch=True)
ax.set_xticks([1,2])
ax.set_xticklabels(['Homogenous (MTAG)', 'Homogenous (JASS)'],rotation=30)
ax.set_ylim([-0.01,1.2])
ax.set_ylabel('observed gain')
result = ttest_ind(similar_val_jass['obs_gain'].values,similar_train['obs_gain'].values,axis=0,equal_var=False,alternative='two-sided')
ax.plot([1, 1, 2-0.01, 2-0.01],[1.09, 1.1, 1.1, 1.09], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(similar_val_jass['obs_gain'].mean()-similar_train['obs_gain'].mean(),2), '%.1E' % result.pvalue)
ax.text(1.5, 1.11, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)

ax=axs['(D)']
ax.boxplot([similar_val['obs_joint'].values,similar_val_jass['obs_joint'].values],notch=True)
ax.set_xticks([1,2])
ax.set_xticklabels(['Homogenous (MTAG)', 'Homogenous (JASS)'],rotation=30)
ax.set_ylim([0,230])
ax.set_ylabel('observed #new associations')
result = ttest_ind(similar_val_jass['obs_joint'].values,similar_train['obs_joint'].values,axis=0,equal_var=False,alternative='two-sided')
ax.plot([1, 1, 2-0.01, 2-0.01],[200, 205, 205, 200], lw=1, c='k')
sig_symbol = 'diff={}, p={}'.format(round(similar_val_jass['obs_joint'].mean()-similar_train['obs_joint'].mean(),2), '%.1E' % result.pvalue)
ax.text(1.5, 206, sig_symbol, ha='center', va='bottom', c='k',fontsize=12)


#ax=axs['(E)']
#names = sorted(set(similar_val.group_names))
#print(names)
#ax.boxplot([similar_val.loc[(similar_val.n_group==1)&(similar_val.group_names==name)]['obs_gain'].values for name in names],vert=False)
#ax.set_yticks(range(1,len(names)+1))
#ax.set_yticklabels([clinical_name[name]+' ({} sets)'.format(len(similar_val.loc[(similar_val.n_group==1)&(similar_val.group_names==name)])) for name in names])
#ax.set_xlabel('observed gain')
#ax.set_title('Homogenous traits')

def setBoxColors(bp,color):
	bp['boxes'][0].set_facecolor(color)
	bp['medians'][0].set_color(color)

ax=axs['(E)']
names = sorted(set(similar_val.group_names))
pos = 1
labels = []
yticks = []
for name in names:
	data1 = similar_val.loc[(similar_val.n_group==1)&(similar_val.group_names==name)]['obs_gain'].values
	bp1 = ax.boxplot(data1, positions =[pos],vert=False,patch_artist=True) 
	setBoxColors(bp1,'red') # MTAG
	data2 = similar_val_jass.loc[(similar_val_jass.n_group==1)&(similar_val_jass.group_names==name)]['obs_gain'].values
	bp2 = ax.boxplot(data2, positions =[pos+1],vert=False,patch_artist=True) 
	setBoxColors(bp2,'blue') # JASS
	labels += [clinical_name[name]+' ({} sets)'.format(len(similar_val.loc[(similar_val.n_group==1)&(similar_val.group_names==name)]))]
	yticks.append(pos + 0.5)
	pos += 3
	result = ttest_ind(data1,data2,axis=0,equal_var=False,alternative='two-sided')
	print('gain',clinical_name[name],result.statistic, result.pvalue,data1.mean()-data2.mean())
ax.set_yticks(yticks)
ax.set_yticklabels(labels)
ax.set_xlabel('observed gain')
ax.set_title('Homogenous traits')

#ax=axs['(F)']
#ax.boxplot([similar_val.loc[(similar_val.n_group==1)&(similar_val.group_names==name)]['obs_joint'].values for name in names],vert=False)
#ax.set_yticks(range(1,len(names)+1))
#ax.set_yticklabels(['' for i in range(len(names))])
#ax.set_xlabel('# of new associations')
#ax.set_title('Homogenous traits')

ax=axs['(F)']
names = sorted(set(similar_val.group_names))
pos = 1
labels = []
yticks = []
for name in names:
	data1 = similar_val.loc[(similar_val.n_group==1)&(similar_val.group_names==name)]['obs_joint'].values
	bp1 = ax.boxplot(data1, positions =[pos], vert=False, patch_artist=True)
	setBoxColors(bp1,'red') # MTAG
	data2 = similar_val_jass.loc[(similar_val_jass.n_group==1)&(similar_val_jass.group_names==name)]['obs_joint'].values
	bp2 = ax.boxplot(data2, positions =[pos+1],vert=False,patch_artist=True) # pink: MTAG, lightblue: JASS
	setBoxColors(bp2,'blue') # JASS
	labels += [clinical_name[name]+' ({} sets)'.format(len(similar_val.loc[(similar_val.n_group==1)&(similar_val.group_names==name)]))]
	yticks.append(pos + 0.5)
	pos += 3
	result = ttest_ind(data1,data2,axis=0,equal_var=False,alternative='two-sided')
	print('#joint',clinical_name[name],result.statistic, result.pvalue,data1.mean()-data2.mean())
ax.set_yticks(yticks)
ax.set_yticklabels(['' for i in range(len(yticks))])
ax.set_xlabel('observed #new associations')
ax.set_title('Homogenous traits')
ax.legend([bp1['boxes'][0],bp2['boxes'][0]], ['MTAG','JASS'],loc='upper right')

for label, ax in axs.items():
        # label physical distance to the left and up:
        trans = mtransforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
        ax.text(0.0, 1.0, label, transform=ax.transAxes + trans,
                fontsize='medium', va='bottom', fontfamily='arial')

print('data-driven:',datadriven['obs_joint'].mean())
print('heterogenous:',dif_val['obs_joint'].mean())
print('very heterogenous:',verydif_val['obs_joint'].mean())
print('similar:',similar_val['obs_joint'].mean())

plt.subplots_adjust(bottom=0.2,wspace=0.2,hspace=0.4,left=0.3)
plt.savefig(output,dpi=300)

