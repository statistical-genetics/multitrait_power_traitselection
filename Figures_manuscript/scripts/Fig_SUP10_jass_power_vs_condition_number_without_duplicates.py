import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import spearmanr,pearsonr
import scipy

df = pd.read_csv("../inputs/JASS_5CVdata-2023-08-01/traitset_jass_5CVcombined_without_duplicates.tsv",delimiter='\t')

print('min condition number:',df.condition_number_rcov.min())
print('max non-inf condition number:', np.nanmax(df.condition_number_rcov.replace(np.inf,np.nan).values))

df = df.fillna({'Joint':0,'Both':0,'Univariate':0,'None':0})
df['%Joint'] = df['Joint']*100 / (df['Both'] + df['Univariate'] + df['Joint'])  #(df['Both'] + df['Univariate'] + df['None'] + df['Joint'])
cn_max = np.ma.masked_invalid(df.condition_number_rcov.values).max()
df.loc[np.isinf(df.condition_number_rcov),'condition_number_rcov'] = cn_max * 2
plt.rcParams.update({'font.family':'arial','font.size':18})
fig, axs = plt.subplots(1,2,figsize=(20, 7))
ax = axs[0]
sns.scatterplot(x=np.log10(df['condition_number_rcov'].values),y=df['Joint'].values,ax=ax)
ax.set_xlabel('log10 condition number')
ax.set_ylabel('Number of new associations')
r,p = spearmanr(np.log10(df['condition_number_rcov'].values),df['Joint'].values)
if p > 0:
	ax.text(0.92,192,"spearman\n"+r"   r={}, p={}".format(round(r,2),'%.1E' % p))
else:
	ax.text(0.92,192,"spearman\n"+r"   r={}, p<{}".format(round(r,2), '%.1E' % scipy.finfo(float).smallest_normal))
print('pval (spearman):',p)
r,p = pearsonr(np.log10(df['condition_number_rcov'].values),df['Joint'].values)
if p > 0:
	ax.text(0.92,172,"pearson\n"+r"   r={}, p={}".format(round(r,2),'%.1E' % p))
else:
	ax.text(0.92,172,"pearson\n"+r"   r={}, p<{}".format(round(r,2), '%.1E' % scipy.finfo(float).smallest_normal))
ax.text(-0.05,200,'(A)')
print('pval (pearson):',p)

ax = axs[1]
sns.scatterplot(x=np.log10(df['condition_number_rcov'].values),y=df['fraction_more_significant_joint_qval'].values,ax=ax)
ax.set_xlabel('log10 condition number')
ax.set_ylabel('JASS gain')
r,p = spearmanr(np.log10(df['condition_number_rcov'].values),df['fraction_more_significant_joint_qval'].values)
if p > 0:
	ax.text(0.92,0.95,"spearman\n"+r"   r={}, p={}".format(round(r,2),'%.1E' % p))
else:
	ax.text(0.92,0.95,"spearman\n"+r"   r={}, p<{}".format(round(r,2), '%.1E' % scipy.finfo(float).smallest_normal))
print('pval (spearman):',p)
r,p = pearsonr(np.log10(df['condition_number_rcov'].values),df['fraction_more_significant_joint_qval'].values)
if p > 0:
	ax.text(0.92,0.85,"pearson\n"+r"   r={}, p={}".format(round(r,2),'%.1E' % p))
else:
	ax.text(0.92,0.85,"pearson\n"+r"   r={}, p<{}".format(round(r,2), '%.1E' % scipy.finfo(float).smallest_normal))
ax.text(-0.05,1,'(B)')
print('pval (pearson):',p)

plt.subplots_adjust(bottom=0.1,left=0.1,wspace=0.15)
plt.savefig('../outputs/Figure_SUP_jass_power_vs_condition_number_without_duplicates.png',dpi=300)
