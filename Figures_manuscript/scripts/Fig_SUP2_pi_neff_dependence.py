import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

df = pd.read_csv("../inputs/72trait_data_2023-07-07.csv",delimiter=',')
df['log10_pi'] = np.log10(df['polygenicity (unadjusted)'])

plt.rcParams.update({'font.family':'arial','font.size':18})
plt.figure(figsize=[6,4])
sns.regplot(x='Neff',y='log10_pi',data=df,ci=95)
plt.ylim([-6,-0.9])
plt.ylabel("$log_{10}$ polygenicity (unadjusted)")
plt.xlabel("effective sample size")

plt.tight_layout()
plt.savefig("../outputs/Figure_SUP_pi_neff_dependence.png",dpi=300)
