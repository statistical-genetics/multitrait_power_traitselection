import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import RandomizedSearchCV
from sklearn.inspection import permutation_importance
from sklearn import preprocessing
from sklearn.metrics import r2_score
from sklearn.svm import SVR

target = 'fraction_more_significant_joint_qval'
model_type = "SVR" # select SVR or RFR 
#model_type = "RFR"

feature_candidate = ['k','log10_avg_distance_cor','log10_mean_gencov','log10_mean_null_phencov','avg_Neff','avg_h2_mixer','avg_perc_h2_diff_region'] 

def generateModel(model_type,tuned_params,X_train_scaled,y_train_scaled):
    # re-fit the model with the tuned hyperparameter and training data
    if model_type == 'RFR':
        model = RandomForestRegressor(n_estimators=tuned_params['n_estimators'],
                            max_features=tuned_params['max_features'],
                            max_depth=tuned_params['max_depth'],
                            min_samples_split=tuned_params['min_samples_split'],
                            min_samples_leaf=tuned_params['min_samples_leaf'],
                            bootstrap=tuned_params['bootstrap'],
                            random_state=1).fit(X_train_scaled,y_train_scaled)
    elif model_type == 'SVR':
        model = SVR(kernel=tuned_params['kernel'],C=tuned_params['C'],
                epsilon=tuned_params['epsilon'],degree=tuned_params['degree']).fit(X_train_scaled,y_train_scaled)
        
    return model

def tuning(model_type,X_train_scaled,y_train_scaled):
    if model_type == 'RFR':
        parameters = {'n_estimators': [5,20,50,100],
                  'max_features':['auto','sqrt'],
                  'max_depth':[int(x) for x in np.linspace(2,100,num=12)],
                  'min_samples_split':[2,5,10],
                  'min_samples_leaf':[1,2,4],
                  'bootstrap':[True,False]}
        print(parameters)
        model = RandomForestRegressor()
        reg = RandomizedSearchCV(model,parameters,n_jobs=-1,random_state=1234)

    elif model_type == 'SVR':
        # a recommendation for the parameter range: https://stackoverflow.com/questions/69669827/tuning-of-hyperparameters-of-svr
        # http://adrem.uantwerpen.be/bibrem/pubs/IJCNN2007.pdf
        parameters = {'kernel': ('linear','rbf','sigmoid','poly'), 'C':[1,10,50,100],'epsilon':[1e-3, 1e-2, 1e-1, 1],'degree':[2,3,4]}
        print(parameters)
        model = SVR()
        reg = RandomizedSearchCV(model,parameters,cv=5,n_jobs=-1,random_state=1234)

    reg.fit(X_train_scaled,y_train_scaled)
    return reg.best_params_

def calc_permutation_importance(model,X_train_scaled,y_train_scaled):
    result = permutation_importance(model, X_train_scaled,y_train_scaled,n_repeats=10,random_state=0)
    print("permutation importance", result.importances_mean)
    return result


pimportances_mean = []
pimportances_std = []
hyperparams = {}
if model_type == 'SVR':
    for i in ['kernel','C','epsilon','degree']:
        hyperparams[i] = []
if model_type == 'RFR':
    for i in ['n_estimators','max_features','max_depth','min_samples_split','min_samples_leaf','bootstrap']:
        hyperparams[i] = []
R2_train = []
R2_val = []
corR_train = []
corR_val = []
corR2_train = []
corR2_val = []
df_jass_training_all = pd.DataFrame()
df_jass_test_all = pd.DataFrame()
for i in [2,3,4,5,6]:
# Data input
    df_jass_training = pd.read_csv('../inputs/traitset_jass_CVtraining{}-newSUMMARY_remove-nan.tsv'.format(i),delimiter='\t')
    df_jass_test = pd.read_csv('../inputs/traitset_jass_CVtest{}-newSUMMARY_remove-nan.tsv'.format(i),delimiter='\t')
    df_jass_training['log10_mean_gencov'] = np.log10(df_jass_training.mean_gencov)
    df_jass_training['log10_mean_null_phencov'] = np.log10(df_jass_training.mean_null_phencov)
    df_jass_training['log10_avg_distance_cor'] = np.log10(df_jass_training.avg_distance_cor)
    df_jass_test['log10_mean_gencov'] = np.log10(df_jass_test.mean_gencov)
    df_jass_test['log10_mean_null_phencov'] = np.log10(df_jass_test.mean_null_phencov)
    df_jass_test['log10_avg_distance_cor'] = np.log10(df_jass_test.avg_distance_cor)

    df_jass_training['CV'] = i
    df_jass_training['train'] = True
    df_jass_test['CV'] = i
    df_jass_test['train'] = False
    df_jass_training_all = pd.concat([df_jass_training_all,df_jass_training],axis=0)
    df_jass_test_all = pd.concat([df_jass_test_all,df_jass_test],axis=0)
df_jass_all = pd.concat([df_jass_training_all,df_jass_test_all],axis=0)

# Apply Scaling on the whole data at once. 
# Using MinMaxScaler to avoid the impact of replications across different CVs on the mean and std values.
X_all = df_jass_all[feature_candidate]
X_scaled_all = preprocessing.MinMaxScaler().fit(X_all).transform(X_all.astype(float))
y_all = df_jass_all[[target]]
y_scaled_all = preprocessing.MinMaxScaler().fit(y_all).transform(y_all.astype(float)).reshape([1,-1])[0]

for i in [2,3,4,5,6]:
    # model trained with the training set
    y_train_scaled = y_scaled_all[((df_jass_all.CV==i)&(df_jass_all.train==True)).values.tolist()]
    X_train_scaled = X_scaled_all[((df_jass_all.CV==i)&(df_jass_all.train==True)).values.tolist(),:]
    tuned_params = tuning(model_type,X_train_scaled,y_train_scaled)
    for k,v in tuned_params.items():
        hyperparams[k].append(v)
    model = generateModel(model_type,tuned_params,X_train_scaled,y_train_scaled)
    result = calc_permutation_importance(model,X_train_scaled,y_train_scaled)
    pimportances_mean.append(result.importances_mean)
    pimportances_std.append(result.importances_std)
    y_pred = model.predict(X_train_scaled)
    R2_train.append(r2_score(y_train_scaled, y_pred))
    corR_train.append(np.corrcoef(y_train_scaled,y_pred)[0,1])
    corR2_train.append(np.corrcoef(y_train_scaled,y_pred)[0,1]**2)

    # calculate R2 comparing the prediction using the above trained model for validation set against the actual values in the validation set
    y_val_scaled = y_scaled_all[((df_jass_all.CV==i)&(df_jass_all.train==False)).values.tolist()]
    X_val_scaled = X_scaled_all[((df_jass_all.CV==i)&(df_jass_all.train==False)).values.tolist(),:]
    y_pred = model.predict(X_val_scaled)
    R2_val.append(r2_score(y_val_scaled, y_pred))
    corR_val.append(np.corrcoef(y_val_scaled,y_pred)[0,1])
    corR2_val.append(np.corrcoef(y_val_scaled,y_pred)[0,1]**2)

pimportances_mean = np.array(pimportances_mean)
pimportances_std = np.array(pimportances_std)
model_data = {} # data to output
# save permutation importance
for i,feature in enumerate(feature_candidate):
    model_data[feature+'_importance_mv'] = pimportances_mean[:,i].tolist()
    model_data[feature+'_importance-std_mv'] = pimportances_std[:,i].tolist()
# save predictive power metrics
model_data['R2train_mv'] = R2_train
model_data['R2val_mv'] = R2_val
model_data['corRtrain_mv'] = corR_train
model_data['corRval_mv'] = corR_val
model_data['corR2train_mv'] = corR2_train
model_data['corR2val_mv'] = corR2_val
# save tuned hyperparameters
for k,v in hyperparams.items():
    model_data[k] = v
df_model = pd.DataFrame(model_data,index=['CV1','CV2','CV3','CV4','CV5'])
df_model.to_csv('../outputs/5foldCV_{}_summary_semilogadjust_log10_excCorVar.csv'.format(model_type),sep=',',na_rep='NA')
