import pandas as pd
import numpy as np
import statsmodels.api as sm
from scipy.stats import pearsonr
from sklearn import preprocessing
from sklearn.metrics import r2_score

target = 'fraction_more_significant_joint_qval'

feature_candidate = ['k','log10_avg_distance_cor','log10_mean_gencov','avg_Neff','avg_h2_mixer','avg_perc_h2_diff_region'] # excluded correlated features
output = '../outputs/gain-true-predicted_jass_linear_pre-selected-features'
output2 = '../outputs/5foldCV_linear_summary_semilogadjust_log10_excCorVar.csv'
output3 = '../outputs/pred_5foldCV_linear_summary_semilogadjust_log10_excCorVar'

# Version with MES
#feature_candidate = ['avg_log10mes_semilogadjust','k','log10_avg_distance_cor','log10_mean_gencov','avg_Neff','avg_h2_mixer'] # excluded correlated features
#output = '../../data/processed/gain-true-predicted_jass_linear_pre-selected-features_wMES'
#output2 = '../../data/processed/5foldCV_linear_summary_semilogadjust_log10_excCorVar_wMES.csv'
#output3 = '../../data/processed/pred_5foldCV_linear_summary_semilogadjust_log10_excCorVar_wMES'

# Version with polygenicity
#feature_candidate = ['avg_log10pi_semilogadjust','k','log10_avg_distance_cor','log10_mean_gencov','avg_Neff','avg_h2_mixer'] # excluded correlated features
#output = '../../data/processed/gain-true-predicted_jass_linear_pre-selected-features_wPI'
#output2 = '../../data/processed/5foldCV_linear_summary_semilogadjust_log10_excCorVar_wPI.csv'
#output3 = '../../data/processed/pred_5foldCV_linear_summary_semilogadjust_log10_excCorVar_wPI'

coefficients = []
pvalues = []
SEs = []
coefficients_univ = []
pvalues_univ = []
SEs_univ = []
R2_train = []
R2_val = []
R2_val_adj = []
corR_train = []
corR_val = []
corR2_train = []
corR2_val = []
corR2_val_adj = []
cor_p_train = []
cor_p_val = []
df_jass_all = pd.DataFrame()

for i in [2,3,4,5,6]:
    # read the saved files
    df_jass_training = pd.read_csv('../inputs/traitset_jass_CVtraining{}-newSUMMARY_remove-nan.tsv'.format(i),delimiter='\t')
    df_jass_test = pd.read_csv('../inputs/traitset_jass_CVtest{}-newSUMMARY_remove-nan.tsv'.format(i),delimiter='\t')

    # transformation of features
    df_jass_training['log10_mean_gencov'] = np.log10(df_jass_training.mean_gencov)
    df_jass_training['log10_mean_null_phencov'] = np.log10(df_jass_training.mean_null_phencov)
    df_jass_training['log10_avg_distance_cor'] = np.log10(df_jass_training.avg_distance_cor)
    max_cn_rcov = np.ma.masked_invalid(df_jass_training.condition_number_rcov.values).max()
    df_jass_training['condition_number_rcov'] = df_jass_training['condition_number_rcov'].replace(np.inf,max_cn_rcov)
    max_cn_gcov = np.ma.masked_invalid(df_jass_training.condition_number_gcov.values).max()
    df_jass_training['condition_number_gcov'] = df_jass_training['condition_number_gcov'].replace(np.inf,max_cn_gcov)
    df_jass_training['log10_condition_number_rcov'] = np.log10(df_jass_training.condition_number_rcov)
    df_jass_training['log10_condition_number_gcov'] = np.log10(df_jass_training.condition_number_gcov)
    df_jass_test['log10_mean_gencov'] = np.log10(df_jass_test.mean_gencov)
    df_jass_test['log10_mean_null_phencov'] = np.log10(df_jass_test.mean_null_phencov)
    df_jass_test['log10_avg_distance_cor'] = np.log10(df_jass_test.avg_distance_cor)
    max_cn_rcov = np.ma.masked_invalid(df_jass_test.condition_number_rcov.values).max()
    df_jass_test['condition_number_rcov'] = df_jass_test['condition_number_rcov'].replace(np.inf,max_cn_rcov)
    max_cn_gcov = np.ma.masked_invalid(df_jass_test.condition_number_gcov.values).max()
    df_jass_test['condition_number_gcov'] = df_jass_test['condition_number_gcov'].replace(np.inf,max_cn_gcov)
    df_jass_test['log10_condition_number_rcov'] = np.log10(df_jass_test.condition_number_rcov)
    df_jass_test['log10_condition_number_gcov'] = np.log10(df_jass_test.condition_number_gcov)

    df_jass_training['CV'] = i
    df_jass_training['train'] = True
    df_jass_test['CV'] = i
    df_jass_test['train'] = False
    df_jass_all = pd.concat([df_jass_all,df_jass_training],axis=0)
    df_jass_all = pd.concat([df_jass_all,df_jass_test],axis=0)


# Apply Scaling on the whole data at once. 
X_all = df_jass_all[feature_candidate]
X_ranges = pd.DataFrame({"minimum_value": X_all.min(), 
              "maximum_value": X_all.max()})

X_ranges.to_csv("../outputs/range_features.tsv", sep="\t")

X_scaled_all = preprocessing.MinMaxScaler().fit(X_all).transform(X_all.astype(float)) #preprocessing.StandardScaler().fit(X_train).transform(X_train.astype(float))
y_all = df_jass_all[[target]]
y_scaled_all = preprocessing.MinMaxScaler().fit(y_all).transform(y_all.astype(float)).reshape([1,-1])[0]



for i in [2,3,4,5,6]:
    # linear model (multivariate) trained with the training set
    y_train_scaled = y_scaled_all[((df_jass_all.CV==i)&(df_jass_all.train==True)).values.tolist()]
    X_train_scaled = X_scaled_all[((df_jass_all.CV==i)&(df_jass_all.train==True)).values.tolist(),:]
    lm = sm.OLS(y_train_scaled,sm.add_constant(X_train_scaled)).fit()
    pvalues.append(lm.pvalues.tolist()[1:])
    coefficients.append(lm.params.tolist()[1:])
    SEs.append(lm.bse.tolist()[1:])
    y_pred = lm.predict(sm.add_constant(X_train_scaled))
    R2_train.append(r2_score(y_train_scaled, y_pred))
    rho,p = pearsonr(y_train_scaled,y_pred)
    corR_train.append(rho)
    cor_p_train.append(p)
    corR2_train.append(corR_train[-1]**2)

    # calculate R2 comparing the prediction using the above trained model for validation set against the actual values in the validation set
    y_val_scaled = y_scaled_all[((df_jass_all.CV==i)&(df_jass_all.train==False)).values.tolist()]
    X_val_scaled = X_scaled_all[((df_jass_all.CV==i)&(df_jass_all.train==False)).values.tolist(),:]
    y_pred = lm.predict(sm.add_constant(X_val_scaled))
    R2_val.append(r2_score(y_val_scaled, y_pred))
    rho,p = pearsonr(y_val_scaled,y_pred)
    corR_val.append(rho)
    cor_p_val.append(p)
    corR2_val.append(corR_val[-1]**2)
    y_pred_adj = np.mean(y_pred) + (y_pred - np.mean(y_pred))*(np.std(y_val_scaled)/np.std(y_pred)) #y_pred * np.std(y_val_scaled)/np.std(y_pred) # adjusting the variance in the predicted gain with sd difference
    rho,p = pearsonr(y_val_scaled,y_pred_adj)
    corR2_val_adj.append(rho**2)
    R2_val_adj.append(r2_score(y_val_scaled,y_pred_adj))

    # output the predicted gains
    with open(output3+'_CV{}.txt'.format(i),'w') as f:
        data = df_jass_all.loc[(df_jass_all.CV==i)&(df_jass_all.train==False)].reset_index(inplace=False)
        f.write('trait,k,predicted_gain,observed_gain,Joint\n')
        for l in range(len(data)):
            string = '{},{},{},{},{}\n'.format(data.iloc[l].trait,data.iloc[l].k,y_pred[l],data.iloc[l][target],data.iloc[l]['Joint'])
            f.write(string)

    # save observed and predicted gains
    with open(output+'_CV{}.txt'.format(i),'w') as f:
        f.write('observed_gain_scaled,predicted_gain_scaled,adjusted_predicted_gain_scaled\n')
        for obs,pred,pred_adj in zip(y_val_scaled,y_pred,y_pred_adj):
            f.write('{},{},{}\n'.format(obs,pred,pred_adj))

    # linear model (univariate) trained with the training set
    coefs_univ = []
    ps_univ = []
    stes_univ = []
    for f,feature in enumerate(feature_candidate):
        lm_univ = sm.OLS(y_train_scaled,sm.add_constant(X_train_scaled[:,f])).fit()
        coefs_univ += lm_univ.params.tolist()[1:]
        ps_univ += lm_univ.pvalues.tolist()[1:]
        stes_univ += lm_univ.bse.tolist()[1:]
    coefficients_univ.append(coefs_univ)
    pvalues_univ.append(ps_univ)
    SEs_univ.append(stes_univ)


pvalues = np.array(pvalues)
SEs = np.array(SEs)
coefficients = np.array(coefficients)
pvalues_univ = np.array(pvalues_univ)
SEs_univ = np.array(SEs_univ)
coefficients_univ = np.array(coefficients_univ)
model_data = {} # data to output
# save linear model summary
for i,feature in enumerate(feature_candidate):
    model_data[feature+'_coef_mv'] = coefficients[:,i].tolist()
    model_data[feature+'_pval_mv'] = pvalues[:,i].tolist()
    model_data[feature+'_SE_mv'] = SEs[:,i].tolist()
    model_data[feature+'_coef_uv'] = coefficients_univ[:,i].tolist()
    model_data[feature+'_pval_uv'] = pvalues_univ[:,i].tolist()
    model_data[feature+'_SE_uv'] = SEs_univ[:,i].tolist()
# save prediction power values
model_data['R2train_mv'] = R2_train
model_data['R2val_mv'] = R2_val
model_data['R2val_adj_mv'] = R2_val_adj
model_data['corRtrain_mv'] = corR_train
model_data['corRval_mv'] = corR_val
model_data['corR2train_mv'] = corR2_train
model_data['corR2val_mv'] = corR2_val
model_data['corR2val_adj_mv'] = corR2_val_adj
model_data['cor_p_train_mv'] = cor_p_train
model_data['cor_p_val_mv'] = cor_p_val
df_model = pd.DataFrame(model_data,index=['CV1','CV2','CV3','CV4','CV5'])
df_model.to_csv(output2,sep=',',na_rep='NA')
